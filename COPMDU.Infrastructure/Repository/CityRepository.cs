﻿using COPMDU.Domain;
using COPMDU.Domain.Entity;
using COPMDU.Infrastructure.Interface;
using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Linq;

namespace COPMDU.Infrastructure.Repository
{
    public class CityRepository : ICityRepository
    {
        private readonly CopMduDbContext _db;

        public CityRepository(CopMduDbContext db)
        {
            _db = db;
        }

        public City Get(string name)
        {
            return _db.City.Where(c => c.Name.Equals(name)).FirstOrDefault();
        }
    }
}
