﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Domain.Entity
{
    public class Ticket
    {
        public int Id { get; set; }
        public string TechnicianUser { get; set; }
        public long Contract { get; set; }
        public int Notification { get; set; }
        public int Cid { get; set; }
        public int IdResolution { get; set; }
        public string Description { get; set; }
        public int AffectedChannels { get; set; }
        public string Status { get; set; }
        public bool ValidStatus { get; set; }
        public bool? ValidSignal { get; set; }
        public int ClosedById { get; set; }
    }
}
